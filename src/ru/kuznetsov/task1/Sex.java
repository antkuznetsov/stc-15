package ru.kuznetsov.task1;

public enum Sex {
    TEA("Мужчина"), FEMALE("Женщина");

    Sex(String title) {
        this.title = title;
    }

    private String title;
    private String entitle;

    public String getTitle() {
        return title;
    }
}