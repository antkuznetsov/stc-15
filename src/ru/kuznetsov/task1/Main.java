package ru.kuznetsov.task1;

import ru.kuznetsov.task3.OuterClass;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;


public class Main {
    public static void main(String[] args) {
        /*Animal animal = new Animal();

        Animal matroskin = new Cat();
        Animal sharik = new Dog();
        Animal gavrusha = new Cow();

        matroskin.makeSound();
        ((Cat) matroskin).drinkMilk();

        sharik.makeSound();
        gavrusha.makeSound();

        *//*Calculator.sum(5, 8);
        Calculator.sum(5.8, 8.8);*//*

        Box box = new Box();

        box.addAnimal(matroskin);
        box.addAnimal(sharik);

        Box bigBox = new Box();
        bigBox.addAnimal(gavrusha);

        System.out.println(matroskin.name);

        Animal people[] = new Animal[3];

        System.out.println(Arrays.toString(Sex.values()));

        people[0] = new Animal("Антон", "Кузнецов", MALE);
        people[1] = new Animal("Артём", "Панасюк", MALE);
        people[2] = new Animal("Татьяна", "Иванова", Sex.valueOf("FEMALE"));

        System.out.println(Arrays.toString(people));

        Animal aux = new Animal("Антон", "Кузнецов", MALE);

        System.out.println(aux.getSex().getTitle());

        Animal animals[] = new Animal[3];
        animals[0] = matroskin;
        animals[1] = sharik;
        animals[2] = gavrusha;

        ArrayList<Integer> arrayList = new ArrayList();

        arrayList.add(5);
        System.out.println(arrayList.size());


        arrayList.remove(Integer.valueOf(5));

        System.out.println(arrayList.size());

        LocalDate ddd = LocalDate.of(2019, Month.APRIL, 22);
        System.out.println(ddd);*/

        OuterClass outer = new OuterClass();
        OuterClass.InnerClass inner = outer.new InnerClass();
    }
}