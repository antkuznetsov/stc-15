package ru.kuznetsov.task1;

public class Cat extends Animal {
    public String name = "Кот";

    @Override
    public void makeSound() {
        System.out.println("Мяу!");
    }

    public void drinkMilk() {
        System.out.println("Ням");
    }
}
