package ru.kuznetsov.Lesson9;

public interface Fallable {
    default void fall() {
        System.out.println("Я падаю из интерфейса Fallable");
    }
}