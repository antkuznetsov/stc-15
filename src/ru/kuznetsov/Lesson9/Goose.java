package ru.kuznetsov.Lesson9;

public class Goose extends Animal implements Flyeble, CanSwim {

    @Override
    public void swim() {
        System.out.println("Могу плавать!");
    }

    @Override
    public void makeSound() {
        System.out.println("Га-га-га!");
    }
}