package ru.kuznetsov.Lesson9;

public class Dog extends Animal {
    @Override
    public void makeSound() {
        System.out.println("Гав!");
    }

    @Override
    public void eat() {
        System.out.println("Ням! Гав!");
    }

    public void barking(){
        System.out.println("Гаааав!");
    }
}