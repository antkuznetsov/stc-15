package ru.kuznetsov.Lesson8;

public class Lesson8 {
    private String title;

    public Lesson8(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}