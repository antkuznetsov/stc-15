package ru.kuznetsov.task3;

public class OuterClass {
    public OuterClass() {
        System.out.println("Создан объект внешнего класса!");
    }

    public class InnerClass {
        public InnerClass() {
            System.out.println("Создан объект внутреннего класса!");
        }
    }
}
