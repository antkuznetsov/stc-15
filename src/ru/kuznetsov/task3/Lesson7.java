package ru.kuznetsov.task3;

import java.time.LocalDate;

public class Lesson7 {
    private static String text;
    private int number;
    private boolean flag;

    public Lesson7(String text, int number) {
        this.text = text;
        this.number = number;
    }

    public Lesson7(String text, int number, boolean flag) {
        this.text = text;
        this.number = number;
        this.flag = flag;
    }

    public static void main(String[] args) {
        //Homework7 h7 = new Homework7();

        LocalDate test = LocalDate.parse("2 февраля 2019");
        
        //System.out.println(h7);

        text = "Строка"; // == сеттер
        System.out.println(text); // == геттер
    }

    private int sum(int a, int b){
        return a + b;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Lesson7{" +
                "text='" + text + '\'' +
                ", number=" + number +
                ", flag=" + flag +
                '}';
    }
}
