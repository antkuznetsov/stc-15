package ru.kuznetsov.task3;

import ru.kuznetsov.Lesson8.Lesson8;
import ru.kuznetsov.Lesson8.Converter;

public class Part2Lesson8 {
    public static void main(String[] args) {
        Lesson8 l8 = new Lesson8("Занятие 8");
        System.out.println(Converter.convert(l8));
    }
}