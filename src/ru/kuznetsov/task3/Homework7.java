package ru.kuznetsov.task3;

public class Homework7 extends Lesson7 {

    public Homework7(String text, int number) {
        super(text, number);
    }

    public Homework7(String text, int number, boolean flag) {
        super(text, number, flag);
    }
}
