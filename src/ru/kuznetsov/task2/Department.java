package ru.kuznetsov.task2;

import java.util.Arrays;

public class Department {
    private String title;
    private Student[] students = new Student[10];

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Department{" +
                "title='" + title + '\'' +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
