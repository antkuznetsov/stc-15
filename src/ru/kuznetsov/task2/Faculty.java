package ru.kuznetsov.task2;

import java.util.Arrays;

public class Faculty {
    private String title;
    private Department[] departments = new Department[5];

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Department[] getDepartments() {
        return departments;
    }

    public void setDepartments(Department[] departments) {
        this.departments = departments;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "title='" + title + '\'' +
                ", departments=" + Arrays.toString(departments) +
                '}';
    }
}
