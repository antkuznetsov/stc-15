package ru.kuznetsov.task2;

import java.time.LocalDate;

public class UniversityApp {
    public static void main(String[] args) {
        Student ivan = new Student("Иван", "Иванов", LocalDate.now());

        ivan.setFirstName("Ваня");
        System.out.println(ivan.getFirstName());

        /*ivan.firstName = "Ваня";
        System.out.println(ivan.firstName);*/

        ivan.setRecordBookNumber(123);

        Faculty f5 = new Faculty();
        Department d53 = new Department();

        d53.setStudents(new Student[]{ivan});

        f5.setDepartments(new Department[]{d53});

        System.out.println(f5);
    }
}
