package ru.kuznetsov.task2;

import java.time.LocalDate;

public class Student extends Person {
    private int recordBookNumber;

    public Student(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }

    public int getRecordBookNumber() {
        return recordBookNumber;
    }

    public void setRecordBookNumber(int recordBookNumber) {
        this.recordBookNumber = recordBookNumber;
    }
}
