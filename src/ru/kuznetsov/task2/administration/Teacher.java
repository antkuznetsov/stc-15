package ru.kuznetsov.task2.administration;

import ru.kuznetsov.task2.Person;

import java.time.LocalDate;

public class Teacher extends Person {
    private int salary;

    public Teacher(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }

    int getSalary() {
        return salary;
    }

    void setSalary(int salary) {
        this.salary = salary;
    }
}