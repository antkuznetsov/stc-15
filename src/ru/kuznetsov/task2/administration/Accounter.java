package ru.kuznetsov.task2.administration;

import ru.kuznetsov.task2.Person;

import java.time.LocalDate;

public class Accounter extends Person {
    public Accounter(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }
}
